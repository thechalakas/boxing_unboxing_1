﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boxing_Unboxing_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //a value type with a value assigned to it
            //this is currently stored in heap memory
            int hello_there = 20;
            //boxing it
            //object type is reference type
            object o = hello_there;
            //now o holds a reference to hello_there contents which are now in heap memory
            //sending this reference type object to another method
            show_boxed_object(o);

            //this is to stop console window from closing
            Console.ReadLine();
        }

        private static void show_boxed_object(object o)
        {
            //I will now 'unbox' the object o to its int counterpart
            int hello_there_again = (int)o;

            Console.WriteLine("the unboxed int value is {0}", hello_there_again);
        }
    }
}
